//
//  OverlaysCollectionViewCell.swift
//  LyrebirdApp
//
//  Created by Yagiz on 12.08.2021.
//

import UIKit
import Kingfisher
class OverlaysCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var overlayNameLabel: UILabel!
    @IBOutlet weak var overlayImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configureCell(model:GetOverlaysResponseModelElement) {
     
        overlayNameLabel.text = model.overlayName
        overlayImageView.kf.setImage(with: URL(string: model.overlayPreviewIconURL ?? ""), placeholder: UIImage.init(named: "placeholder"), options: nil, completionHandler: nil)
        
    }
    

}
