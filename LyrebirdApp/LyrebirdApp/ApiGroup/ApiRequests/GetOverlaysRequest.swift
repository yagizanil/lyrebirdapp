//
//  GetOverlaysRequest.swift
//  LyrebirdApp
//
//  Created by Yagiz on 12.08.2021.
//

import Foundation
class GetOverlaysRequest: APIRequest {
    override init() {
        super.init()
      
        super.initAPIRequest(endPoint: getOverlaysRequestUrl, httpMethod: HTTPMethod.get, bodyParameters: nil, urlParameters: nil)
    }
    override func request(completion: @escaping (Codable) -> Void, failed: @escaping (ResponseError) -> Void) {
        BaseRequest.request(_APIRequest: self, completion: { responseData,responseHeaders  in
            guard let initialize = try? JSONDecoder().decode(GetOverlaysResponseModel.self, from: responseData) else {
                failed(ResponseError.decodingFailed)
                return
                }
                completion(initialize)
                }, failed: { error in
                    failed(error)
                })
    }
}
