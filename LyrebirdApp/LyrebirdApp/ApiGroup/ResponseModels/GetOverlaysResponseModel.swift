//
//  GetOverlaysResponseModel.swift
//  LyrebirdApp
//
//  Created by Yagiz on 12.08.2021.
//

import Foundation

struct GetOverlaysResponseModelElement: Codable {
    var overlayID: Int?
    var overlayName: String?
    var overlayPreviewIconURL, overlayURL: String?

    enum CodingKeys: String, CodingKey {
        case overlayID = "overlayId"
        case overlayName
        case overlayPreviewIconURL = "overlayPreviewIconUrl"
        case overlayURL = "overlayUrl"
    }
}

typealias GetOverlaysResponseModel = [GetOverlaysResponseModelElement]
