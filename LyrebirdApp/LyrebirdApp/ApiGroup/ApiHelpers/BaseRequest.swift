//
//  BaseRequest.swift
//  LyrebirdApp
//
//   Created by Yagiz on 12.08.2021.
//

import Alamofire
import Foundation

class BaseRequest:NSObject {

    class func request(_APIRequest: APIRequest, completion: @escaping (_ responseData: Data, _ responseHeaders: [String: String]) -> Void, failed: @escaping (_ error: ResponseError) -> Void) {
        
        let request = getRequest(_APIRequest: _APIRequest)
        if request == nil {
            failed(ResponseError.requestFailed)
            return
        }
        AF.request(request!).responseJSON { response in
            
            switch response.result {
            case .success(_):
                
                guard let responseData = response.data else {
                    failed(ResponseError.noData)
                    return
                }
                guard let responseHeaders = response.response?.allHeaderFields as? [String: String] else {
                    completion(responseData, [:])
                    failed(ResponseError.noData)
                    return
                }
                
                completion(responseData, responseHeaders)

            case .failure(let error):

                print(error.localizedDescription)
                if let httpResponseCode = response.response?.statusCode {
                    if httpResponseCode >= 500 {
                        failed(ResponseError.httpError)
                        return
                    }
                }
                if error._code == NSURLErrorTimedOut {
                    failed(ResponseError.requestTimedOut)
                }
                else if error._code == NSURLErrorBadURL
                {
                    failed(ResponseError.badUrlException)
                }
                else if error._code == NSURLErrorUnsupportedURL {
                    failed(ResponseError.unsupportedUrl)
                }
                else if error._code == NSURLErrorCancelled {
                    failed(ResponseError.cancelled)
                }
                else if error._code == NSURLErrorUnknown {
                    failed(ResponseError.unknownError)
                }
                else if error._code == NSURLErrorCannotFindHost {
                    failed(ResponseError.cannotFindHost)
                }
                else if error._code == NSURLErrorNotConnectedToInternet {
                    failed(ResponseError.notConnectedToInternet)
                }
                else if error._code == NSURLErrorResourceUnavailable {
                    failed(ResponseError.resourceUnavailable)
                }
                else if error._code == NSURLErrorClientCertificateRejected {
                    failed(ResponseError.clientCertificateRejected)
                }
                else if error._code == NSURLErrorBadServerResponse {
                    failed(ResponseError.badServerResponse)
                }
                else if error._code == NSURLErrorNetworkConnectionLost {
                    failed(ResponseError.networkConnectionLost)
                }
            }
            
        }
     
        
        
    }
    class func getRequest(_APIRequest:APIRequest) -> URLRequest? {
        
        do {
        let url = URL(string: _APIRequest.endPoint)
            if url?.description == nil {
                return nil
            }
            var request = URLRequest(url: url!)
        request.headers = _APIRequest.headers
            request.httpMethod = _APIRequest.httpMethod.rawValue
            if _APIRequest.httpMethod == .get {
                
            } else {
            request.httpBody = try JSONSerialization.data(withJSONObject: _APIRequest.bodyParameters ?? [:], options: [])
            }
        return request
        }
        catch {
           return nil
        }
        
    }
    
    
    
    
    
    
}
