//
//  APIRequest.swift
//  LyrebirdApp
//
//  Created by Yagiz on 12.08.2021.
//

import Foundation
import Alamofire
public typealias HTTPBodyParameters = [String : Any]?

public typealias HTTPUrlParameters = [String : Any]?

class APIRequest: NSObject {
    var endPoint: String!
    
    var httpMethod: HTTPMethod!
    var headers:HTTPHeaders!
    var bodyParameters: Parameters?
    var urlParameters: HTTPUrlParameters?
    
    func initAPIRequest(endPoint: String, httpMethod: HTTPMethod, bodyParameters: Parameters?, urlParameters: HTTPUrlParameters?) {
        self.endPoint = Utilities.endPoint + endPoint
        self.httpMethod = httpMethod
        headers = self.setHeaders()
        
        
    
        self.bodyParameters = bodyParameters
        self.urlParameters = urlParameters
        setUrlParameters()
        print(self.endPoint)
    }
    
    func request(completion: @escaping (_ T : Codable) -> Void, failed: @escaping (_ error : ResponseError) -> Void) {}
    
    private func setUrlParameters() {
        guard urlParameters != nil else {
            return
        }
        
        var index = 0
        
        for (key, value) in urlParameters!! {
            endPoint.append(key)
            endPoint.append("=")
            endPoint.append(String(describing: value))
            
            if index < urlParameters!!.count - 1 {
                index = index + 1
                endPoint.append("&")
            }
        }
    }
    
    func convertToDictionary(data: Data? = Data()) -> Parameters? {
        do {
            let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String:Any]
            return json
        } catch {
            print("Something went wrong")
        }
        return nil
    }
    private func setHeaders() -> HTTPHeaders {
        var headerSet = [HTTPHeader]()
        headerSet.append(HTTPHeader(name:"application/json", value: "Content-Type"))

            
        return HTTPHeaders.init(headerSet)
    }
}
