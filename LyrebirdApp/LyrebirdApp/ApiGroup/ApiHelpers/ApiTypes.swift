//
//  ApiTypes.swift
//  LyrebirdApp
//
//   Created by Yagiz on 12.08.2021.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"

}
enum ResponseError : String {
    case noData = "Data is nil"
    case decodingFailed = "Decoding failed"
    case needsToSetCookie = "Cookie could not found in header"
    case httpError = "HTTP Error Occured"
    case requestTimedOut = "Request timed out"
    case badUrlException = "Bad Url Exception"
    case unsupportedUrl = "Unsupported Url Exception"
    case cancelled = "Request cancelled"
    case unknownError = "Unknown error occured"
    case requestFailed = "Request Failed"
    case cannotFindHost = "Cannot find host"
    case notConnectedToInternet = "Check your internet connection"
    case resourceUnavailable = "Resource is unavailable"
    case clientCertificateRejected = "Client certificate rejected"
    case badServerResponse = "Bad server response"
    case networkConnectionLost = "Network Connection Lost"
}
