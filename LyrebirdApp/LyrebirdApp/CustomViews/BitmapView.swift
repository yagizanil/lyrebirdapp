//
//  BitmapView.swift
//  LyrebirdApp
//
//  Created by Yagiz on 12.08.2021.
//

import Foundation
import UIKit
import CoreGraphics
import Accelerate
class BitmapView: UIView {
  
    var overlayFrame:CGRect! {
        didSet {
            self.setNeedsDisplay()
        }
    }
    var translation:CGPoint!
    var overlayImage:UIImage?
    var bitmapImage:UIImage?
    var context: CGContext?
    var isPanStarted = false
    var isOverlay = false
    var isGesture = false
    var isHistogram = false
    var lastScale:CGFloat!
    var graphView:GraphView!
    func histogram(image: UIImage) -> (red: [UInt], green: [UInt], blue: [UInt], alpha: [UInt]) {

    let img: CGImage = CIImage(image: image)!.cgImage!
     
    let imgProvider: CGDataProvider = img.dataProvider!
    let imgBitmapData: CFData = imgProvider.data!
    var imgBuffer = vImage_Buffer(
        data: UnsafeMutableRawPointer(mutating: CFDataGetBytePtr(imgBitmapData)),
        height: vImagePixelCount(img.height),
        width: vImagePixelCount(img.width),
        rowBytes: img.bytesPerRow)
    
       
   
    var binZero = [vImagePixelCount](repeating: 0, count: 256)
    var binOne = [vImagePixelCount](repeating: 0, count: 256)
    var binTwo = [vImagePixelCount](repeating: 0, count: 256)
    var binThree = [vImagePixelCount](repeating: 0, count: 256)
        
    binZero.withUnsafeMutableBufferPointer { zeroPtr in
        binOne.withUnsafeMutableBufferPointer { onePtr in
            binTwo.withUnsafeMutableBufferPointer { twoPtr in
                binThree.withUnsafeMutableBufferPointer { threePtr in
                    
                    var histogramBins = [zeroPtr.baseAddress, onePtr.baseAddress,
                                         twoPtr.baseAddress, threePtr.baseAddress]
                    
                    histogramBins.withUnsafeMutableBufferPointer {
                        histogramBinsPtr in
                        let error = vImageHistogramCalculation_ARGB8888(
                            &imgBuffer,
                            histogramBinsPtr.baseAddress!,
                            vImage_Flags(kvImageNoFlags))
                        
                        guard error == kvImageNoError else {
                            fatalError("Error calculating histogram: \(error)")
                        }
                        
                       
                    }
                }
            }
        }
    }
       
    return (binZero, binOne, binTwo, binThree)
       
    }
    
    var histogramArray:(red: [UInt], green: [UInt], blue: [UInt], alpha: [UInt])?
    func drawHistogram() {
         
        self.isHistogram = true
        self.setNeedsDisplay()
        
    }
    func removeHistogram() {
        self.isHistogram = false
        if graphView != nil {
            self.graphView.removeFromSuperview()
            self.graphView = nil
        }
        self.setNeedsDisplay()
    }
    override func draw(_ rect: CGRect) {
        
        if context == nil {
        context = UIGraphicsGetCurrentContext()
        } else {
           
        }
        context?.setShouldAntialias(false)
        
        context?.clear(self.bounds)
        
       
        let size = CGRect(x: 0, y: 0, width: rect.width, height: rect.height)
        if isHistogram {
            histogramArray = histogram(image: self.asImage())
            bitmapImage?.draw(in: size)
            if overlayImage != nil && overlayFrame != nil {
            overlayImage?.draw(in: overlayFrame)
            }
            
            guard let hist = histogramArray else { return }
                
                let count = hist.alpha.count
                    
                  

          struct _xData { var i : Int; var x : CGFloat }
          
          
          let idx = Array(0..<count)
          
            let width:CGFloat  = 256
            let height:CGFloat = 100
          
            let xscale : CGFloat = width  / CGFloat(count-1)
            let _ : CGFloat = height / 100
                    
                    
            let x = idx.map { _xData(i:$0, x: CGFloat($0) * xscale) }
                    
                   
            let red = x.map { CGPoint(x: $0.x, y: CGFloat( CGFloat(hist.red[$0.i])) ) }
            
            var redGraphData:[[String:Int]] = []
           
            for (index,item) in red.enumerated() {
                redGraphData.append(["\(index)":Int(item.y) < 0 ? Int(item.y)  : Int(item.y) ])
            }
            
            if graphView == nil {
                self.graphView = GraphView(frame: CGRect(x: rect.maxX - 128, y: rect.maxY - 100, width: 256, height: 100), data:redGraphData)
            } else {
                self.graphView.removeFromSuperview()
                self.graphView = GraphView(frame: CGRect(x: rect.maxX - 128, y: rect.maxY - 100, width: 256, height: 100), data: redGraphData)
            }
            graphView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
            self.addSubview(graphView)
            
          
            
        }
        if isGesture {
            
           
            
            bitmapImage?.draw(in: size)
            
            overlayImage?.draw(in: overlayFrame)
           
        } else if isOverlay {
            
            if overlayImage != nil {
                
                let siz = CGRect(x: 0, y: 0, width: rect.width, height: rect.height)
                bitmapImage?.draw(in: siz)
               
                let overlaySize = CGRect(x: self.frame.midX -  50, y: self.bounds.midY - 50, width: 100, height: 100)
             
                self.overlayFrame = overlaySize
                overlayImage?.draw(in: overlayFrame)
               
                self.isOverlay = false
            }
            
            
        } else {
            if bitmapImage != nil {
                if !isHistogram {
                    
                    
                
                    bitmapImage?.draw(in: size)
                    if self.overlayImage != nil && self.overlayFrame != nil {
                        overlayImage?.draw(in: overlayFrame)
                    }
                }
                
            }
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isMultipleTouchEnabled = true
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func drawImage(with image:UIImage) {
        self.bitmapImage = image
        self.setNeedsDisplay()
        
    }
    func setOverlayImage(with image:UIImage) {
        self.overlayImage = image
        self.isOverlay = true
        let overlaySize = CGRect(x: self.frame.midX -  50, y: self.bounds.midY - 50, width: 100, height: 100)
     
        self.overlayFrame = overlaySize
        self.setNeedsDisplay()
        self.initGestureTuples()
       
     
    }
    func removeOverlayImage() {
        self.overlayImage = nil
        self.setNeedsDisplay()
    }
    func initGestureTuples() {
  
        self.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(dragGestureRecognizer(event:))))
       self.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(pinchGestureRecognizer(event:))))
        
    }
   
    @objc func pinchGestureRecognizer(event:UIPinchGestureRecognizer) {
        isGesture = true
      
        if event.state == .recognized {
            if lastScale != nil {
                event.scale = lastScale
            }

        }
        if event.state == .began {
            if lastScale != nil {
                event.scale = lastScale
            }
        
        if event.location(in: self).x > self.overlayFrame.minX && event.location(in: self).x < self.overlayFrame.maxX && event.location(in: self).y > self.overlayFrame.minY && event.location(in: self).y < self.overlayFrame.maxY {
            var scale:CGFloat = 0.0
           
            if lastScale != nil {
             scale = lastScale / event.scale
            } else {
             scale = event.scale
            lastScale = event.scale
            }
            self.overlayFrame = CGRect(x: self.overlayFrame.minX - ((100 * scale) - self.overlayFrame.width) / 2   , y: self.overlayFrame.minY - ((100 * scale) - self.overlayFrame.height) / 2  , width: 100 * scale, height: 100 * scale)
        
        } else {
            
        }
        } else if event.state == .changed {
            
            let scale = event.scale
            self.overlayFrame = CGRect(x: self.overlayFrame.minX - ((100 * scale) - self.overlayFrame.width) / 2   , y: self.overlayFrame.minY - ((100 * scale) - self.overlayFrame.height) / 2  , width: 100 * scale, height: 100 * scale)
            lastScale = event.scale
            
        } else if event.state == .ended {
            
            
            
        }
    }
    
   
    @objc func dragGestureRecognizer(event:UIPanGestureRecognizer) {
        self.isGesture = true
        
        let tempTranslation = event.translation(in: self)
       
                
        if event.state == .ended {
            self.isGesture = false
            self.isPanStarted = false
        }
        else if event.state == .began {
         if event.location(in: self).x > self.overlayFrame.minX && event.location(in: self).x < self.overlayFrame.maxX && event.location(in: self).y > self.overlayFrame.minY && event.location(in: self).y < self.overlayFrame.maxY {
            UIView.animate(withDuration: 0.5, animations: {
                self.overlayFrame = CGRect(x: self.overlayFrame.minX + tempTranslation.x, y: self.overlayFrame.minY +  tempTranslation.y, width: self.overlayFrame.width, height: self.overlayFrame.height)
            })
            
            isPanStarted = true
        }
        } else if event.state == .changed {
            if isPanStarted {
                UIView.animate(withDuration: 0.5, animations: {
                    self.overlayFrame = CGRect(x: self.overlayFrame.minX + tempTranslation.x - self.translation.x, y: self.overlayFrame.minY +  tempTranslation.y - self.translation.y, width: self.overlayFrame.width, height: self.overlayFrame.height)
                })
            }
        }
        translation = event.translation(in: self)
   
    }
    func makeTransparent(image: UIImage) -> UIImage? {
        guard let rawImage = image.cgImage else { return nil}
        let colorMasking: [CGFloat] = [255, 255, 255, 255, 255, 255]
        UIGraphicsBeginImageContext(image.size)
        
        if let maskedImage = rawImage.copy(maskingColorComponents: colorMasking),
            let context = UIGraphicsGetCurrentContext() {
            context.translateBy(x: 0.0, y: image.size.height)
            context.scaleBy(x: 1.0, y: -1.0)
            context.draw(maskedImage, in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
            let finalImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return finalImage
        }
        
        return nil
    }
}
extension UIView {

    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}

