//
//  CustomButton.swift
//  LyrebirdApp
//
//  Created by Yagiz on 13.08.2021.
//

import Foundation
import UIKit
@IBDesignable class CustomButtonWithRadiusAndShadow:UIButton {
    
    @IBInspectable var cornerRadius:CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    @IBInspectable var borderWidth:CGFloat = 0 {
         didSet {
             self.layer.borderWidth = self.borderWidth
         }
     }
    @IBInspectable var borderColor:UIColor = .black {
          didSet {
            self.layer.borderColor = self.borderColor.cgColor
          }
      }
   
    override  func awakeFromNib() {
        setup()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    convenience init() {
        self.init(frame:CGRect.zero)
        setup()
    }
    private func setup() {
        layer.shadowColor = UIColor.white.withAlphaComponent(0.2).cgColor
              layer.shadowOpacity = 1
              layer.shadowOffset = .zero
              layer.shadowRadius = 10
               layer.cornerRadius = 12
        
        
    }
    
    
}
