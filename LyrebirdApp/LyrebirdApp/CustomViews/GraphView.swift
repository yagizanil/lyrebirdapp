//
//  GraphView.swift
//  LyrebirdApp
//
//  Created by Yagiz on 14.08.2021.
//

import Foundation
import UIKit
import QuartzCore

class GraphView: UIView {
    
    private var redData = [[String: Int]]()
    private var greenData = [[String: Int]]()
    private var blueData = [[String: Int]]()
    private var context : CGContext?
    
    private let padding     : CGFloat = 30
    private var graphWidth  : CGFloat = 0
    private var graphHeight : CGFloat = 0
    private var axisWidth   : CGFloat = 0
    private var axisHeight  : CGFloat = 0
    private var everest     : CGFloat = 0
    
    
 
    
    var xMargin         : CGFloat = 20
    var originLabelText : String?
    var originLabelColor = UIColor.black
    
    required init(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(frame: CGRect, data: [[String: Int]]) {
        
        super.init(frame: frame)
        backgroundColor = UIColor.clear
        self.redData = data
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        context = UIGraphicsGetCurrentContext()
        
       
        graphWidth = (rect.size.width - padding) - 10
        graphHeight = rect.size.height
        axisWidth = rect.size.width - 10
        axisHeight = (rect.size.height )
        
        
        for (index, point) in redData.enumerated() {
            let keys = Array(redData[index].keys)
            let currKey = keys.first!
            if CGFloat(point[currKey]!) > everest {
                everest = CGFloat(Int(ceilf(Float(point[currKey]!) / 25) * 25))
            }
        }
        if everest == 0 {
            everest = 25
        }

        let redPointPath = CGMutablePath()
        let redFirstPoint = redData[0][redData[0].keys.first!]
        let redInitialY : CGFloat = ceil((CGFloat(redFirstPoint!) * (axisHeight / everest))) - 10
        let redInitialX : CGFloat = 0
        redPointPath.move(to: CGPoint(x: redInitialX, y: graphHeight - redInitialY))
        
       
        for (_, value) in redData.enumerated() {
            plotPoint(point: [value.keys.first!: value.values.first!], path: redPointPath)
        }
        
        
        context!.addPath(redPointPath)
        context!.setLineWidth(2)
        context!.setStrokeColor(UIColor.red.cgColor)
        context!.fillPath()
        
       
   
    }
    
    
    
    func plotPoint(point : [String: Int], path: CGMutablePath) {
    
        
        let interval = Int(graphWidth - xMargin * 2) / 100
        let pointValue = point[point.keys.first!]
        let yposition : CGFloat = ceil((CGFloat(pointValue!) * (axisHeight / everest))) - 10

        var index = 0
        for (ind, value) in redData.enumerated() {
            if point.keys.first! == value.keys.first! && point.values.first! == value.values.first! {
                index = ind
            }
        }
        let xposition = CGFloat(interval * index)
        
        
       
        path.addLine(to: CGPoint(x: xposition, y: graphHeight - yposition))

    }

}
