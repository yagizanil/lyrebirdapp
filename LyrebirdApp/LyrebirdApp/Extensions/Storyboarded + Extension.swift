//
//  Storyboarded + Extension.swift
//  LyrebirdApp
//
//  Created by Yagiz on 12.08.2021.
//

import Foundation
import UIKit
protocol Storyboarded {
    static func instantiate(with tStoryboard : Storyboards) -> Self
}

public enum Storyboards : String {
    case main = "Main"
 
}
extension Storyboarded where Self: UIViewController {
    static func instantiate(with tStoryboard : Storyboards) -> Self {
        let fullName = NSStringFromClass(self)
        let className = fullName.components(separatedBy: ".")[1]
        let storyboard = UIStoryboard(name: tStoryboard.rawValue, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: className) as! Self
    }
}
