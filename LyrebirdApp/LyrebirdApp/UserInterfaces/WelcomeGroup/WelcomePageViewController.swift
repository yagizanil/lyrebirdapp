//
//  WelcomePageViewController.swift
//  LyrebirdApp
//
//  Created by Yagiz on 13.08.2021.
//

import UIKit

class WelcomePageViewController: UIViewController {

    var imagePicker = UIImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func pickImageAction(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                            print("Button capture")

                            imagePicker.delegate = self
                            imagePicker.sourceType = .savedPhotosAlbum
                            imagePicker.allowsEditing = false

                            present(imagePicker, animated: true, completion: nil)
          }
    }
}

extension WelcomePageViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let newImg = pickedImage.resize(targetSize: CGSize(width: self.view.frame.width, height: 250))
            let vc = MainPageController.instantiate(with: .main)
            vc.selectedImage = newImg
            self.navigationController?.pushViewController(vc, animated: true)
           
            self.dismiss(animated: true, completion: { () -> Void in
                 
            })
        }
    }
}
