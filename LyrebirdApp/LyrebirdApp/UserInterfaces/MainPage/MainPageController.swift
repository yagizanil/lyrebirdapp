//
//  ViewController.swift
//  LyrebirdApp
//
//  Created by Yagiz on 12.08.2021.
//

import UIKit
import Kingfisher
class MainPageController: UIViewController,Storyboarded {

    var imagePicker = UIImagePickerController()
    var hasHistogram = false {
        didSet {
            if hasHistogram {
                self.drawHistogramImageView.image = UIImage(systemName: "square.split.2x1.fill")
            } else {
                self.drawHistogramImageView.image = UIImage(systemName: "square.split.2x1")
            }
        }
    }
    @IBAction func dismissAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var drawHistogramImageView: UIImageView! {
        didSet {
            self.drawHistogramImageView.isUserInteractionEnabled = true
            self.drawHistogramImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(drawHistogram)))
        }
    }
    var isHistogramActive = false
    var pickedOverlayImage:UIImage? {
        didSet {
            self.baseView.setOverlayImage(with: pickedOverlayImage!)
        }
    }
    var selectedImage:UIImage!
    var selectedImageUrl = "" {
        didSet {
            let downloader = ImageDownloader.default
            
            downloader.downloadImage(with: URL(string: selectedImageUrl)!) { result in
                switch result {
                case .success(let value):
                    print(value.image)
                    self.pickedOverlayImage = value.image
                case .failure(let error):
                    print(error)
                }
            }
            
        }
    }
    
    @IBOutlet weak var baseView: BitmapView!
    @IBOutlet weak var mainPageCollectionView: UICollectionView!
    lazy var mainPageViewModel: MainPageViewModel = {
        
        return MainPageViewModel()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainPageViewModel.requestFinished = {
            
            self.drawCollectionView()
            
        }
        
        mainPageViewModel.getOverlays()
        self.baseView.drawImage(with: self.selectedImage)
    }
    @IBAction func saveImage(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(baseView.asImage(), self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    func drawCollectionView() {
        
        self.mainPageCollectionView.delegate = self
        self.mainPageCollectionView.dataSource = self
        self.mainPageCollectionView.register(UINib.init(nibName: "OverlaysCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OverlaysCollectionViewCell")
        
        
        
    }
    @objc func drawHistogram() {
        if hasHistogram {
            self.baseView.removeHistogram()
            hasHistogram = false
        } else {
        self.baseView.drawHistogram()
            hasHistogram = true
        }
    }
    
    
}

extension MainPageController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainPageViewModel.getSearchResultResponseModel?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OverlaysCollectionViewCell", for: indexPath) as? OverlaysCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.configureCell(model: (self.mainPageViewModel.getSearchResultResponseModel?[indexPath.item])!)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedImageUrl = self.mainPageViewModel.getSearchResultResponseModel?[indexPath.item].overlayURL ?? ""
    }
    

    
    
}
