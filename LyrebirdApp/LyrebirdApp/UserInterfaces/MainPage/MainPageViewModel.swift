//
//  MainPageViewModel.swift
//  LyrebirdApp
//
//  Created by Yagiz on 12.08.2021.
//

import Foundation


class MainPageViewModel {
    
    var getSearchResultResponseModel:GetOverlaysResponseModel?
   
    var requestFinished:(()->())?
    var errorThrowed:((String)->())?
    
    func getOverlays() {
        do {
             let cacheObject = try UserDefaults.standard.getObject(forKey: "overlays", castTo: GetOverlaysResponseModel.self)
            self.getSearchResultResponseModel = cacheObject
            requestFinished?()
            return
        } catch {
            
        }
        let request = GetOverlaysRequest()
        
        request.request(completion: {object in
            
            if let respObj =  object as? GetOverlaysResponseModel {
                self.getSearchResultResponseModel = respObj
                self.requestFinished?()
                do {
                   try UserDefaults.standard.setObject(respObj, forKey: "overlays")
                } catch {
                    
                }
               
            } else {
                self.errorThrowed?("Decoding Failed")
            }
            
        }, failed: {error in
            
            self.errorThrowed?(error.rawValue)
            
        })
        
        
    }
}
